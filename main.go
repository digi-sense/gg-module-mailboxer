package main

import (
	"bitbucket.org/digi-sense/gg-core"
	_ "bitbucket.org/digi-sense/gg-core-x"
	"fmt"
)

const (
	Name = "MAILBOXER"
)

func main() {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := gg.Strings.Format("[panic] APPLICATION %s ERROR: %s. Workspace: %s",
				Name, r, gg.Paths.GetWorkspacePath())

			fmt.Println(message)
		}
	}()

}
