module bitbucket.org/digi-sense/gg-module-mailboxer

go 1.16

require (
	bitbucket.org/digi-sense/gg-core v0.1.35
	bitbucket.org/digi-sense/gg-core-x v0.1.35
	github.com/cention-sany/utf7 v0.0.0-20170124080048-26cad61bd60a
	github.com/emersion/go-imap v1.2.0
	github.com/emersion/go-sasl v0.0.0-20211008083017-0b9dcfb154ac // indirect
	github.com/gogs/chardet v0.0.0-20191104214054-4b6791f73a28
	golang.org/x/net v0.0.0-20211216030914-fe4d6282115f
	golang.org/x/text v0.3.7
)
