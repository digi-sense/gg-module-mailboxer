package mailboxer_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_vcal"
	"bitbucket.org/digi-sense/gg-module-mailboxer/mailboxer_mime"
	"bytes"
	"io"
	"os"
	"strings"
	"time"
)

type MailboxerMessageInvite struct {
	Name                string               `json:"name"`
	Version             string               `json:"version"`
	Vendor              string               `json:"vendor"`
	Product             string               `json:"product"`
	Method              string               `json:"method"`
	TimeZone            string               `json:"timezone"`
	EventUID            string               `json:"event_uid"`
	EventOrganizer      string               `json:"event_organizer"`
	EventSummary        string               `json:"event_summary"`
	EventDescription    string               `json:"event_description"`
	EventUrl            string               `json:"event_url"`
	EventLocation       string               `json:"event_location"`
	EventStatus         string               `json:"event_status"`
	EventIsCancelled    bool                 `json:"event_is_cancelled"`
	EventStartAt        time.Time            `json:"event_start_at"`
	EventEndAt          time.Time            `json:"event_end_at"`
	EventStartAtInItaly time.Time            `json:"event_start_at_in_italy"`
	EventEndAtInItaly   time.Time            `json:"event_end_at_in_italy"`
	EventAttendees      []string             `json:"event_attendees"`
	EventDuration       time.Duration        `json:"event_duration"`
	EventLinkMeeting    string               `json:"event_link_meeting"`
	EventIsRecurrent    bool                 `json:"event_is_recurrent"`
	EventSlots          []*gg_vcal.RRuleSlot `json:"event_slots"`

	content string
}

// ---------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
// ---------------------------------------------------------------------------------------------------------------------

// NewMailboxerMessageInvite creates an invite only if passed data contains an envelope with a VCALENDAR invite message
func NewMailboxerMessageInvite(data interface{}) (*MailboxerMessageInvite, error) {
	if nil != data {
		if v, b := data.([]byte); b {
			return readInviteFromBytes(v)
		}
		if v, b := data.(io.Reader); b {
			return readInvite(v)
		}
		if v, b := data.(string); b {
			if gg.Paths.IsFilePath(v) {
				return readInviteFromFile(v)
			} else {
				return readInviteFromBytes([]byte(v))
			}
		}
	}
	return nil, nil
}

func NewMailboxerMessageInviteFromContent(content []byte) (*MailboxerMessageInvite, error) {
	response := new(MailboxerMessageInvite)
	err := response.Parse(content)
	if nil != err {
		return nil, err
	}
	return response, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MailboxerMessageInvite) IsValid() bool {
	if nil != instance && len(instance.content) > 0 {
		return !gg.Dates.IsZero(instance.EventStartAt) && !gg.Dates.IsZero(instance.EventEndAt)
	}
	return false
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func readInviteFromFile(filename string) (*MailboxerMessageInvite, error) {
	r, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	return readInvite(r)
}

func readInviteFromBytes(data []byte) (*MailboxerMessageInvite, error) {
	return readInvite(bytes.NewReader(data))
}

func readInvite(r io.Reader) (*MailboxerMessageInvite, error) {
	env, err := mailboxer_mime.ReadEnvelope(r)
	if nil == err {
		// lookup into attachments for invite.ics
		if len(env.Attachments) > 0 {
			for _, part := range env.Attachments {
				if isCalendar(part) {
					return NewMailboxerMessageInviteFromContent(part.Content)
				}
			}
		}
		// lookup into parts for invite.ics
		if len(env.Inlines) > 0 {
			for _, part := range env.Inlines {
				if isCalendar(part) {
					return NewMailboxerMessageInviteFromContent(part.Content)
				}
			}
		}
		if len(env.OtherParts) > 0 {
			for _, part := range env.OtherParts {
				if isCalendar(part) {
					return NewMailboxerMessageInviteFromContent(part.Content)
				}
			}
		}
	}
	return nil, nil
}

func isCalendar(part *mailboxer_mime.Part) bool {
	if nil != part {
		if len(part.FileName) > 0 {
			name := part.FileName
			ext := strings.ToLower(gg.Paths.ExtensionName(name))
			if ext == "ics" || ext == "ical" {
				return true
			}
		}
		content := string(part.Content)
		return strings.Index(content, "VCAL") > -1
	}
	return false
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MailboxerMessageInvite) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *MailboxerMessageInvite) Parse(content []byte) error {
	calendar, err := gg.VCal.Parse(content)
	if nil != err {
		return err
	}
	instance.content = calendar.String()

	instance.Name = calendar.Name()
	instance.Version = calendar.Version()
	instance.Method = calendar.Method()
	instance.Product = calendar.ProdId()
	instance.Vendor = calendar.ProdVendor()
	if nil != calendar.TimeZone() {
		instance.TimeZone = calendar.TimeZone().Value()
	}
	if len(calendar.Events()) > 0 {
		event := calendar.Events()[0]
		instance.EventUID = event.Id()
		instance.EventOrganizer = event.Organizer()
		instance.EventSummary = event.Summary()
		instance.EventDescription = event.Description()
		instance.EventUrl = event.LinkMeeting()
		instance.EventLocation = event.Location()
		instance.EventStatus = event.Status()
		instance.EventIsCancelled = event.IsStatusCancelled()
		instance.EventLinkMeeting = event.LinkMeeting()
		instance.EventDuration = event.Duration()
		startAt, e := event.GetStartAt()
		if nil == e {
			instance.EventStartAt = startAt
			instance.EventStartAtInItaly = gg.Dates.AdjustDaylight(startAt, "Europe/Rome")
		}
		endAt, e := event.GetEndAt()
		if nil == e {
			instance.EventEndAt = endAt
			instance.EventEndAtInItaly = gg.Dates.AdjustDaylight(endAt, "Europe/Rome")
		}
		instance.EventAttendees = event.AttendeesEmails()

		rrule := event.RRule()
		if nil != rrule {
			instance.EventIsRecurrent = true
			instance.EventSlots = event.GetSlots(rrule.Until)
		} else {
			instance.EventIsRecurrent = false
			instance.EventSlots = []*gg_vcal.RRuleSlot{gg_vcal.NewRRuleSlot(event.Id(), startAt, endAt)}
		}
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------
