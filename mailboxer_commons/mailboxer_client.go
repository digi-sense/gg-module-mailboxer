package mailboxer_commons

import (
	"bitbucket.org/digi-sense/gg-core"
)

type MailBoxerClient struct {
	settings *MailboxerClientSettings
	driver   IDriver
}

func NewMailBoxerClient(params ...interface{}) (*MailBoxerClient, error) {
	instance := new(MailBoxerClient)
	instance.settings = new(MailboxerClientSettings)
	switch len(params) {
	case 1:
		// settings
		if s, b := params[0].(string); b {
			if gg.Regex.IsValidJsonObject(s) {
				err := gg.JSON.Read(s, &instance.settings)
				if nil != err {
					return nil, err
				}
				return instance, nil
			} else {
				// try loading data from file
				text, err := gg.IO.ReadTextFromFile(s)
				if nil != err {
					return nil, err
				}
				return NewMailBoxerClient(text)
			}
		} else if settings, b := params[0].(*MailboxerClientSettings); b {
			err := instance.settings.LoadFromText(gg.JSON.Stringify(settings))
			if nil != err {
				return nil, err
			}
			return instance, nil
		} else if settings, b := params[0].(MailboxerClientSettings); b {
			err := instance.settings.LoadFromText(gg.JSON.Stringify(settings))
			if nil != err {
				return nil, err
			}
			return instance, nil
		} else if settings, b := params[0].(map[string]interface{}); b {
			err := instance.settings.LoadFromText(gg.JSON.Stringify(settings))
			if nil != err {
				return nil, err
			}
			return instance, nil
		}
	default:
		return nil, ErrorMismatchConfiguration
	}
	return instance, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	MailBoxerClient
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MailBoxerClient) String() string {
	m := map[string]interface{}{
		"settings": instance.settings,
	}
	return gg.Convert.ToString(m)
}

func (instance *MailBoxerClient) Open() error {
	if nil == instance.driver {
		driver, err := getDriver(instance.settings)
		if nil != err {
			return err
		}
		instance.driver = driver
	}
	return instance.driver.Open()
}

func (instance *MailBoxerClient) Close() error {
	if nil != instance.driver {
		err := instance.driver.Close()
		instance.driver = nil
		return err
	}
	return nil
}

func (instance *MailBoxerClient) ListMailboxes() ([]*MailboxInfo, error) {
	if nil != instance.driver {
		return instance.driver.ListMailboxes()
	}
	return nil, nil
}

func (instance *MailBoxerClient) GetMailboxFlags(mailboxName string) ([]string, error) {
	if nil != instance.driver {
		return instance.driver.GetMailboxFlags(mailboxName)
	}
	return nil, nil
}

func (instance *MailBoxerClient) ReadMailbox(mailboxName string, onlyNew bool) ([]*MailboxerMessage, error) {
	if nil != instance.driver {
		return instance.driver.ReadMailbox(mailboxName, onlyNew)
	}
	return nil, nil
}

func (instance *MailBoxerClient) ReadMessage(seqNum interface{}) (*MailboxerMessage, error) {
	if nil != instance.driver {
		return instance.driver.ReadMessage(seqNum)
	}
	return nil, nil
}
func (instance *MailBoxerClient) MarkMessageAsSeen(mailbox string, seqNum interface{}) error {
	if nil != instance.driver {
		return instance.driver.MarkMessageAsSeen(mailbox, seqNum)
	}
	return nil
}
func (instance *MailBoxerClient) MarkMessageAsAnswered(mailbox string, seqNum interface{}) error {
	if nil != instance.driver {
		return instance.driver.MarkMessageAsAnswered(mailbox, seqNum)
	}
	return nil
}
func (instance *MailBoxerClient) MarkMessageAsDeleted(mailbox string, seqNum interface{}) error {
	if nil != instance.driver {
		return instance.driver.MarkMessageAsDeleted(mailbox, seqNum)
	}
	return nil
}
func (instance *MailBoxerClient) MarkMessageAsFlagged(mailbox string, seqNum interface{}) error {
	if nil != instance.driver {
		return instance.driver.MarkMessageAsFlagged(mailbox, seqNum)
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
//	S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func getDriver(settings *MailboxerClientSettings) (IDriver, error) {
	dtype := settings.Type
	switch dtype {
	case "imap":
		return NewDriverImap(settings)
	}
	return nil, gg.Errors.Prefix(ErrorDriverNotFound, dtype+": ")
}
