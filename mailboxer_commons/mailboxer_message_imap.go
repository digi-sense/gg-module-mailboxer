package mailboxer_commons

import "github.com/emersion/go-imap"

func parseImap(source *imap.Message, target *MailboxerMessage) {
	if nil != source && nil != target {
		target.SeqNum = source.SeqNum
		target.InternalDate = source.InternalDate
		if nil == target.Header {
			target.Header = NewMailboxerMessageHeader()
		}
		if nil == target.Body {
			target.Body = NewMailboxerMessageBody()
		}

		// ENVELOPE
		if len(target.Header.MessageId) == 0 {
			envelope := source.Envelope
			if nil != envelope {
				target.Header.MessageId = envelope.MessageId
				target.Header.Subject = envelope.Subject
				target.Header.Date = envelope.Date
				target.Header.ParentMessageId = envelope.InReplyTo // parent MessageId

				// REPLY-TO
				target.Header.ReplyTo = make([]*MailboxerAddress, 0)
				for _, a := range envelope.ReplyTo {
					address := &MailboxerAddress{
						PersonalName: a.PersonalName,
						AtDomainList: a.AtDomainList,
						MailboxName:  a.MailboxName,
						HostName:     a.HostName,
					}
					target.Header.ReplyTo = append(target.Header.ReplyTo, address)
				}
				// SENDER
				target.Header.Sender = make([]*MailboxerAddress, 0)
				for _, a := range envelope.Sender {
					address := &MailboxerAddress{
						PersonalName: a.PersonalName,
						AtDomainList: a.AtDomainList,
						MailboxName:  a.MailboxName,
						HostName:     a.HostName,
					}
					target.Header.Sender = append(target.Header.Sender, address)
				}
				// FROM
				target.Header.From = make([]*MailboxerAddress, 0)
				for _, a := range envelope.From {
					address := &MailboxerAddress{
						PersonalName: a.PersonalName,
						AtDomainList: a.AtDomainList,
						MailboxName:  a.MailboxName,
						HostName:     a.HostName,
					}
					target.Header.From = append(target.Header.From, address)
				}
				// TO
				target.Header.To = make([]*MailboxerAddress, 0)
				for _, a := range envelope.To {
					address := &MailboxerAddress{
						PersonalName: a.PersonalName,
						AtDomainList: a.AtDomainList,
						MailboxName:  a.MailboxName,
						HostName:     a.HostName,
					}
					target.Header.To = append(target.Header.To, address)
				}
				// CC
				target.Header.Cc = make([]*MailboxerAddress, 0)
				for _, a := range envelope.Cc {
					address := &MailboxerAddress{
						PersonalName: a.PersonalName,
						AtDomainList: a.AtDomainList,
						MailboxName:  a.MailboxName,
						HostName:     a.HostName,
					}
					target.Header.Cc = append(target.Header.Cc, address)
				}
				// BCC
				target.Header.Bcc = make([]*MailboxerAddress, 0)
				for _, a := range envelope.Bcc {
					address := &MailboxerAddress{
						PersonalName: a.PersonalName,
						AtDomainList: a.AtDomainList,
						MailboxName:  a.MailboxName,
						HostName:     a.HostName,
					}
					target.Header.Bcc = append(target.Header.Bcc, address)
				}
			}
		}

		// BODY STRUCTURE
		if nil != source.BodyStructure {
			// The children parts, if multipart.
			parseImapBodyStructure(source.BodyStructure, source.Body, target)
		}
	}
}

func parseImapBodyStructure(source *imap.BodyStructure, body map[*imap.BodySectionName]imap.Literal, targetMessage *MailboxerMessage) {
	if nil == targetMessage.Body {
		targetMessage.Body = new(MailboxerMessageBody)
	}
	targetBody := targetMessage.Body
	// copy body structure into target output message
	imapMergeBodyStructure(source, targetBody)

	if len(body) > 0 {
		for _, r := range body {
			_ = parseBody(r, targetMessage)
		}
	}

}

func imapMergeBodyStructure(source *imap.BodyStructure, target *MailboxerMessageBody) {
	target.Id = source.Id
	target.Description = source.Description
	target.MIMEType = source.MIMEType
	target.MIMESubType = source.MIMESubType
	target.Params = source.Params
	target.Encoding = source.Encoding
	target.Size = source.Size
	target.Extended = source.Extended
	target.MD5 = source.MD5
	target.Disposition = source.Disposition
	target.DispositionParams = source.DispositionParams
	target.Language = source.Language
	target.Location = source.Location
	target.PartsCount = len(source.Parts)
	target.Multipart = len(source.Parts) > 0
}
