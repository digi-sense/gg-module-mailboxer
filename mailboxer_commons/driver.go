package mailboxer_commons

type IDriver interface {
	Open() error
	Close() error
	ListMailboxes() ([]*MailboxInfo, error)
	GetMailboxFlags(mailboxName string) ([]string, error)
	ReadMailbox(mailboxName string, onlyNew bool) ([]*MailboxerMessage, error)
	ReadMessage(uid interface{}) (*MailboxerMessage, error)
	MarkMessageAsSeen(mailbox string, uid interface{}) error
	MarkMessageAsAnswered(mailbox string, uid interface{}) error
	MarkMessageAsDeleted(mailbox string, uid interface{}) error
	MarkMessageAsFlagged(mailbox string, uid interface{}) error
}
