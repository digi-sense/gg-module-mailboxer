package mailboxer_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-module-mailboxer/mailboxer_mime"
)

func parseMime(envelope *mailboxer_mime.Envelope, target *MailboxerMessage) {
	if nil != envelope && nil != target {
		if nil == target.Header {
			target.Header = NewMailboxerMessageHeader()
		}

		// header
		target.Header.MessageId = envelope.GetHeader("message-id")
		target.Header.Subject = envelope.GetHeader("subject")
		target.Header.Date, _ = gg.Dates.ParseAny(envelope.GetHeader("date"))
		target.Header.ParentMessageId = envelope.GetHeader("parent-id")

		// body
		if nil == target.Body {
			target.Body = NewMailboxerMessageBody()
			target.Body.HTML = envelope.HTML
			target.Body.Text = envelope.Text
		}

	}
}
