package mailboxer_commons

import (
	"bitbucket.org/digi-sense/gg-core"
	_ "embed"
)

// ---------------------------------------------------------------------------------------------------------------------
//	ClientSettingsAuth
// ---------------------------------------------------------------------------------------------------------------------

type MailboxerClientSettingsAuth struct {
	User string `json:"user"`
	Pass string `json:"pass"`
}

// ---------------------------------------------------------------------------------------------------------------------
//	MailboxerClientSettings
// ---------------------------------------------------------------------------------------------------------------------

type MailboxerClientSettings struct {
	Type string                       `json:"type"`
	Host string                       `json:"host"`
	Port int                          `json:"port"`
	Tls  bool                         `json:"tls"`
	Auth *MailboxerClientSettingsAuth `json:"auth"`
}

func (instance *MailboxerClientSettings) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *MailboxerClientSettings) LoadFromFile(filename string) error {
	text, err := gg.IO.ReadTextFromFile(filename)
	if nil != err {
		return err
	}
	return instance.LoadFromText(text)
}

func (instance *MailboxerClientSettings) LoadFromText(text string) error {
	return gg.JSON.Read(text, &instance)
}

// ---------------------------------------------------------------------------------------------------------------------
//	S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func NewMailboxerClientSettings(stype, shost, iport, suser, spass interface{}) *MailboxerClientSettings {
	return &MailboxerClientSettings{
		Type: gg.Convert.ToString(stype),
		Host: gg.Convert.ToString(shost),
		Port: gg.Convert.ToInt(iport),
		Auth: &MailboxerClientSettingsAuth{
			User: gg.Convert.ToString(suser),
			Pass: gg.Convert.ToString(spass),
		},
	}
}
