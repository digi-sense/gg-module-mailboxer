package mailboxer_commons

import (
	"bitbucket.org/digi-sense/gg-core"
)

type MailboxInfo struct {
	// The mailbox attributes.
	Attributes []string `json:"attributes"`
	// The server's path separator.
	Delimiter string `json:"delimiter"`
	// The mailbox name.
	Name string `json:"name"`
}

func (instance *MailboxInfo) String() string {
	return gg.JSON.Stringify(instance)
}

