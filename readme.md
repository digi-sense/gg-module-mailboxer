# MailBoxer #

![](./icon_128.png)

Email in Go made easy

![](./iso-email.png)

## How to Use ##

To use just call:

```
go get bitbucket.org/digi-sense/gg-module-mailboxer@latest
```

### Versioning ###

Sources are versioned using git tags:

```
git tag v0.1.9
git push origin v0.1.9
```