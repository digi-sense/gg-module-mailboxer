package mailboxer

import (
	_ "bitbucket.org/digi-sense/gg-core"
	_ "bitbucket.org/digi-sense/gg-core-x"
	"bitbucket.org/digi-sense/gg-module-mailboxer/mailboxer_commons"
)

func NewMessage(args ...interface{}) (m *mailboxer_commons.MailboxerMessage, err error) {
	m = mailboxer_commons.NewMailboxerMessage()
	if len(args) == 1 {
		err = m.Parse(args[0])
	}
	return
}

func NewClient(params ...interface{}) (*mailboxer_commons.MailBoxerClient, error) {
	return mailboxer_commons.NewMailBoxerClient(params...)
}
